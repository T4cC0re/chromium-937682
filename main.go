package main

import (
	"crypto/rand"
	"fmt"
	"github.com/sirupsen/logrus"
	"net/http"
	"runtime"
)

func GenerateString(n int) string {
	b := make([]byte, n)
	_, _ = rand.Read(b)
	return fmt.Sprintf("%x", b)
}

type Handler struct {
}

var log = logrus.New()

func (h Handler) ServeHTTP(w http.ResponseWriter, origRequest *http.Request) {

	if origRequest.Host == "a.demo.t4cc0.re" {
		w.Header().Set("location", fmt.Sprintf("https://%s.demo.t4cc0.re", GenerateString(8)))
		w.WriteHeader(302)
		return
	}

	_, hasHttp2Pusher := w.(http.Pusher)

	_, _ = w.Write(
		[]byte(
			fmt.Sprintf(
				`
<!doctype html>
<html>
<head>
 <title>Chromium 937682 demo</title>
</head>
<body>
  <p>Demo for <a href="https://bugs.chromium.org/p/chromium/issues/detail?id=937682">Chromium 937682</a></p>
  <p>Client sent SNI: %s</p>
  <p>Client sent Host: %s</p>
  <p>Client is connected via HTTP2: %t</p>
  <p>server compiled with Go version: %s</p>
</body>
</html>
`,
				origRequest.TLS.ServerName,
				origRequest.Host,
				hasHttp2Pusher,
				runtime.Version(),
			),
		),
	)

	if origRequest.TLS.ServerName != origRequest.Host {
		log.WithField("SNI", origRequest.TLS.ServerName).WithField("Host", origRequest.Host).Info("SNI and host mismatch!")
	}
}

func main() {
	cert := "/etc/ssl/private/demo.t4cc0.re/fullchain.pem"
	key := "/etc/ssl/private/demo.t4cc0.re/privkey.pem"
	h := Handler{}
	log.Fatal(http.ListenAndServeTLS("0.0.0.0:443", cert, key, h))
}
